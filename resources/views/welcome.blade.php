<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Confettiz Demo</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="container">
            <form method="post" action="/save" enctype="multipart/form-data">
                @csrf
                <h3>Please fill up the form</h3>
                <p>Please hover on the input filed labels for the available values. Once the attributes matche our existing templates, it will process and generate a unique audio file for you everytime, Otherwise it will return default song.</p>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name" title="aron, abigail, adam, alan, albert, alexander, alexas, alice, amanda, ambar, amy">Your Name</label>
                            <input type="text" class="form-control" placeholder="aron, abigail, adam, alan, albert, alexander, alexas, alice, amanda, ambar, amy" id="name" name="name" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="age" title="2, 3, 4, 5, 6">Your Age</label>
                            <input type="text" class="form-control" placeholder="2, 3, 4, 5, 6" id="age" name="age" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="food" title="cake, chicken soup, chicken nuggets, chocolate, cookies">Favorite Food</label>
                            <input type="text" class="form-control" placeholder="cake, chicken soup, chicken nuggets, chocolate, cookies" id="food" name="food">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="interest" title="computer, dancing, entertaining, family times, biking, bowling, camping, coloring, art & crafts, legos, dressup">Your Interest</label>
                            <input type="text" class="form-control" placeholder="computer, dancing, entertaining, family times, biking, bowling, camping, coloring, art & crafts, legos, dressup" id="interest" name="interest">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="state" title="alabama, alaska, arizona, arkinsos, california, colorado">Your State</label>
                            <input type="text" class="form-control" placeholder="alabama, alaska, arizona, arkinsos, california, colorado" id="state" name="state">
                        </div>
                    </div>
                </div>         
          
                <button type="submit" class="btn btn-primary">Generate Your Song</button>
            </form>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <h3>Original Template</h3>
                        <audio controls>
                            <source src="{{ 'uploads/demo-files/sample-1.mp3' }}" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>

                @if(session()->has('name'))
                    <div class="col-md-4">
                        <div class="form-group">
                            <h3>Audio for {{ session('name') }}</h3>
                            <audio controls>
                                <source src="{{ '/uploads/demo-files/' . session('name') . '.mp3' }}" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                        </div>
                    </div>
                @else 
                    <div class="col-md-4">
                        <div class="form-group">
                            <h4>Please select the attributes and if we have in our database you can see hear it from here.</h4>
                        </div>
                    </div>
                @endif

                @if(session()->has('audio'))
                    <div class="col-md-4">
                        <div class="form-group">
                            <h3>Your Song in ready</h3>
                            <p>File URL (with background music): {{ session('audio')[0] }}</p>
                            <audio controls>
                                <source src="{{ session('audio')[0] }}" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                        </div>

                        <div class="form-group">
                            <p>File URL (without background music): {{ session('audio')[1] }}</p>
                            <audio controls>
                                <source src="{{ session('audio')[1] }}" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                        </div>
                    </div>
                @else 
                    <div class="col-md-4">
                        <div class="form-group">
                            <h4>Your Song is not ready yet</h4>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </body>
</html>
