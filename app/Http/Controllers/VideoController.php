<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\StoreVideoRequest;
use App\Jobs\ConvertVideoForDownloading;
use App\Jobs\ConvertVideoForStreaming;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
use ProtoneMedia\LaravelFFMpeg\Exporters\EncodingException;

class VideoController extends Controller
{
    public function store(StoreVideoRequest $request)
    {
        $video = Video::create([
            'disk'          => 'videos_disk',
            'original_name' => $request->video->getClientOriginalName(),
            'path'          => $request->video->store('videos', 'videos_disk'),
            'title'         => $request->title,
        ]);

        $this->dispatch(new ConvertVideoForDownloading($video));
        $this->dispatch(new ConvertVideoForStreaming($video));

        return response()->json([
            'id' => $video->id,
        ], 201);
    }

    public function index()
    {
        return view('welcome');
    }

    public function save(Request $request)
    {
        $slugged_name       = $this->slugify($request->name);
        $slugged_age        = $this->slugify($request->age);

        if(file_exists('uploads/demo-files/' . $slugged_name . '.mp3') 
            && file_exists('uploads/demo-files/' . $slugged_age . '.mp3'))
        {
            $url =  $this->audioTest($slugged_name, $slugged_age);
            return back()->with('audio', $url)->with('name', $slugged_name);
        }else{
            $url = 'uploads/demo-files/sample-1.mp3';
            return back()->with('audio', $url);
        }
    }

    public function audioTest($name = '', $age = '')
    {
        // $start      = \FFMpeg\Coordinate\TimeCode::fromSeconds(33);
        // $clipFilter = new \FFMpeg\Filters\Audio\AudioClipFilter($start, \FFMpeg\Coordinate\TimeCode::fromSeconds(21));
        
        // FFMpeg::fromDisk('upload')
        //     ->open('/demo-files/sample-1.mp3')
        //     ->addFilter($clipFilter)
        //     ->export()
        //     ->toDisk('upload')
        //     ->inFormat(new \FFMpeg\Format\Audio\Mp3)
        //     ->save('/demo-files/sample-1-4.mp3');

        try {
            $name_template_url              = 'demo-files/' . $name . '.mp3';
            $age_template_url               = 'demo-files/' . $age . '.mp3';

            $audio_merge_arr    = ['demo-files/sample-1-1.mp3', 'demo-files/sample-1-2.mp3', 'demo-files/sample-1-3.mp3', 'demo-files/sample-1-4.mp3'];

            $audio_merge_arr_final  = [];
            $tmp                    = 0;
            foreach($audio_merge_arr as $key => $audio_merge_arr_tmp)
            {
                $audio_merge_arr_final[$tmp]    = $audio_merge_arr_tmp;

                if($key == 0){
                    $audio_merge_arr_final[++$tmp]  = $name_template_url;
                    $audio_merge_arr_final[++$tmp]  = $age_template_url;
                }else if($key == 1){
                    $audio_merge_arr_final[++$tmp]  = $name_template_url;
                }else if($key == 2){
                    $audio_merge_arr_final[++$tmp]  = $name_template_url;
                }                

                ++$tmp;
            }

            $file_name = rand();

            FFMpeg::fromDisk('upload')
                ->open($audio_merge_arr_final)
                ->export()
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->concatWithTranscoding($hasVideo = false, $hasAudio = true)
                ->toDisk('upload')
                ->inFormat(new \FFMpeg\Format\Audio\Mp3)
                ->save('/' . $file_name . '.mp3');

            $file_path = '/var/www/html/confettiz/confettiz/public/uploads/' . $file_name . '.mp3';
            $overlay = '/var/www/html/confettiz/confettiz/public/uploads/demo-files/sample-2.mp3';
            $output = array();
            $return = 0;
            $new_file_name = rand();
            $result1 = exec('ffmpeg -i ' . $file_path . ' -i ' . $overlay . ' -filter_complex amix=inputs=2:duration=longest /var/www/html/confettiz/confettiz/public/uploads/' . $new_file_name . '.mp3', $output, $return); 
            
            
            // $result1 = exec('ffmpeg -i ' . $file_path . ' -i ' . $overlay . ' -filter_complex \
            // "[0:a:0]volume=0.8:precision=fixed[a0]; \
            //  [0:a:1]volume=0.2:precision=fixed[a1]; \
            //  [a0][a1]amerge=inputs=2,pan=stereo:FL<c0+c2+c4:FR<c1+c3+c5[a]" \
            // -map 0:v -map "[a]" -c:v libx264 -preset slow -crf 23 /var/www/html/confettiz/confettiz/public/uploads/' . $new_file_name . '.mp3', $output, $return); 
            
            if ($return != 0)
            {
                dd($output, $return, $file_name, $result1);
            }
            else
            {
                $url[0] = 'uploads/' . $new_file_name . '.mp3';
                $url[1] = 'uploads/' . $file_name . '.mp3';
                return  $url;
            }

            // rename(storage_path('app') . '/public/sample/' . $file_name . '.mp3', public_path() . '/uploads/mix/' . $file_name . '.mp3');

        } catch (EncodingException $exception) {

            $command = $exception->getCommand();
            $errorLog = $exception->getErrorOutput();

            return $errorLog;
        }

    }

    public function slugify($name = '')
    {
        $username   = $name;
        $username   = preg_replace("#[[:punct:]]#", "-", $username);
        $slug       = Str::slug($username, '-');

        if($slug == ''){
            return 1;
        }

        return $slug;
    }
}
